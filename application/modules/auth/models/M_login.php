<?php 
/**
 * 
 */
class M_login extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function cekLogin($table, $data) {
		$query = $this->db->get_where($table, $data);
		if ($query->num_rows() > 0) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function getData($table, $data) {
		return $this->db->get_where($table, $data)->row_object();
	}
}
?>