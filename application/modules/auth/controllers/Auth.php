<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('m_login'));
	}

	public function index(){
		$data['content'] = 'template/content';
		$this->load->view('v_login', $data);
	}

	public function register(){

		$data['content'] = 'v_register';
		$data['image'] = $this->captcha();
		$this->load->view('v_login', $data);
	}

	public function captcha()
	{
		$config = array(
			'img_path'  => './captcha/',
			'img_url'  => base_url().'captcha/',
			'img_width'  => 300,
			'img_height' => 50,
			'expiration' => 7200
		);
   // create captcha image
		$cap = create_captcha($config);
   // store image html code in a variable
		$image = $cap['image'];
   // store the captcha word in a session
		$this->session->set_userdata('captcha_key', $cap['word']);
		return $image;
	}

	public function add()
	{
		$this->form_validation->set_rules('namalengkap', 'nama lengkap', 'required');
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'password', 'required|min_length[6]');
		$this->form_validation->set_rules('nohp', 'nomor telepon', 'required|numeric');
		$this->form_validation->set_rules('captcha', 'captcha', 'required');
		$key = $this->session->userdata('captcha_key');
		$namalengkap = $this->input->post('namalengkap');
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$password = sha1($this->input->post('password'));
		$nohp = $this->input->post('nohp');
		$captcha = $this->input->post('captcha');

		$data = array(
			'user_namalengkap' => $namalengkap, 
			'user_username' => $username, 
			'user_email' => $email, 
			'user_password' => $password, 
			'user_nohp' => $nohp, 
			'user_captcha' => $captcha, 
		);

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata($data);
			$this->register();
		} else if ($captcha != $key) {
			$this->session->set_flashdata($data);
			$this->session->set_flashdata('pesan', 'Captcha anda salah.');
			$this->register();
		} else {
			$this->db->insert('t_user', $data);
			$this->session->set_flashdata('success', 'Registrasi berhasil.');
			return $this->index();
		}
	}

	public function do_login() {
		$this->form_validation->set_rules('username', 'username', 'required');
		$this->form_validation->set_rules('password', 'password', 'required|min_length[6]');

		$data = array(
			'user_username' => $this->input->post('username'),
			'user_password' => sha1($this->input->post('password')));
		$result = $this->m_login->cekLogin('t_user', $data);

		if ($this->form_validation->run() == FALSE) {
			return $this->index();
		} elseif ($result == TRUE) {
			$data_user = $this->m_login->getData('t_user', $data);
			$data_login = array(
				'username' => $data_user->user_username,
			);
			$this->session->set_userdata($data_login);
			redirect('todo/todoapp');
		} else {
			$this->session->set_flashdata('konfirmasiusername', 'username anda salah.');
			$this->session->set_flashdata('konfirmasipassword', 'password anda salah.');
			return $this->index();
		}
	}

	public function do_logout()
	{
		$this->session->sess_destroy();
		return $this->index();
	}
}