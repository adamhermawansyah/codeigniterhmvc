<div id="login">
    <h3 class="text-center text-white pt-5">Login form</h3>
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
                    <?php } ?>
                    <form id="login-form" class="form" action="<?= site_url('auth/do_login') ?>" method="post">
                        <h3 class="text-center text-info">Login</h3>
                        <div class="form-group">
                            <label for="username" class="text-info">Username:</label><br>
                            <input type="text" name="username" id="username" class="form-control" placeholder="username">
                            <a style="color: red;"><?= form_error('username') ?></a>
                            <a style="color: red;"><?= $this->session->flashdata('konfirmasiusername') ?></a>
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-info">Password:</label><br>
                            <input type="password" name="password" id="password" class="form-control" placeholder="password">
                            <a style="color: red;"><?= form_error('password') ?></a>
                            <a style="color: red;"><?= $this->session->flashdata('konfirmasipassword') ?></a>
                        </div>
                        <div class="form-group">
                            <!-- <label for="remember-me" class="text-info"><span>Remember me</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label> -->
                            <br>
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="Login">
                        </div>
                        <div id="register-link" class="text-right">
                            <a href="<?= site_url('auth/register') ?>" class="text-info">Register here</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>