<div id="register">
    <h3 class="text-center text-white pt-5">Register form</h3>
    <div class="container">
        <div id="register-row" class="row justify-content-center align-items-center">
            <div id="register-column" class="col-md-6">
                <div id="register-box" class="col-md-12">
                    <form id="register-form" class="form" action="<?= site_url('auth/add') ?>" method="post">
                        <h3 class="text-center text-info">Register</h3>
                        <div class="form-group">
                            <label for="namalengkap" class="text-info">Nama Lengkap:</label><br>
                            <input type="text" name="namalengkap" id="namalengkap" class="form-control" placeholder="nama lengkap" value="<?= $this->session->flashdata('user_namalengkap'); ?>">
                            <a style="color: red;"><?= form_error('namalengkap') ?></a>
                        </div>
                        <div class="form-group">
                            <label for="username" class="text-info">Username:</label><br>
                            <input type="text" name="username" id="username" class="form-control" placeholder="username" value="<?= $this->session->flashdata('user_username'); ?>">
                            <a style="color: red;"><?= form_error('username') ?></a>
                        </div>
                        <div class="form-group">
                            <label for="email" class="text-info">Email:</label><br>
                            <input type="email" name="email" id="email" class="form-control" placeholder="email" value="<?= $this->session->flashdata('user_email'); ?>">
                            <a style="color: red;"><?= form_error('email') ?></a>
                        </div>
                        <div class="form-group">
                            <label for="username" class="text-info">Password:</label><br>
                            <input type="password" name="password" id="password" class="form-control" placeholder="password">
                            <a style="color: red;"><?= form_error('password') ?></a>
                        </div>
                        <div class="form-group">
                            <label for="nohp" class="text-info">Nomor Telepon:</label><br>
                            <input type="text" name="nohp" id="nohp" class="form-control" placeholder="nomor telepon" value="<?= $this->session->flashdata('user_nohp'); ?>">
                            <a style="color: red;"><?= form_error('nohp') ?></a>
                        </div>
                        <div class="form-group">
                            <p><?= $image ?></p>
                            <label for="captcha" class="text-info">Captcha:</label><br>
                            <input type="text" name="captcha" id="captcha" class="form-control" placeholder="captcha" value="<?= $this->session->flashdata('user_captcha'); ?>">
                            <a style="color: red;"><?= form_error('captcha') ?></a>
                            <a style="color: red;"><?= $this->session->flashdata('pesan'); ?></a>
                        </div>
                        <div class="form-group">
                            <!-- <label for="remember-me" class="text-info"><span>Remember me</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label><br> -->
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="Submit">
                            <a href="<?= site_url('auth') ?>" class="text-info">Sign In</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>