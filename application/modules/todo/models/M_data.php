<?php 
/**
 * 
 */
class M_data extends CI_Model
{

	private $table = 't_todo';
	
	function __construct()
	{
		parent::__construct();
	}

	public function getData($data, $status = NULL, $id = NULL)
	{
		if ($id == NULL) {
			return $this->db->select($data)
			->order_by('todo_id', 'ASC')
			->get_where($this->table, $status)->result();
		} else if ($status == NULL) {
			return $this->db->select($data)->get_where($this->table, $id)->row_object();
		} else {
			return $this->db->select($data)
			->where_in($status, $id)
			->order_by('todo_id', 'ASC')
			->get($this->table)->result();
		}
	}

	public function updateProgress() {
		return $this->db->query("UPDATE t_todo SET Todo_statustask = 'done' WHERE (DATEDIFF(CURDATE(), Todo_tanggalduedate) > 1) AND Todo_statustask = 'progress'");
	}

	public function updateData($data, $key, $id) {
		return $this->db->update($this->table, $data, array($key => $id));
	}
}
?>