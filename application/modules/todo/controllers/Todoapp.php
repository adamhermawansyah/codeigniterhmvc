<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Todoapp extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model(array('m_data'));
	}

	public function index(){
		$data['task_list'] = $this->m_data->getData('Todo_id, Todo_nametask, Todo_tanggalduedate, Todo_deskripsi, Todo_statustask', 'Todo_statustask', array('new', 'progress'));
		$data['task_done'] = $this->m_data->getData('Todo_id, Todo_nametask, Todo_tanggalduedate, Todo_deskripsi, Todo_statustask', array('Todo_statustask' => 'done'));
		$data['task_archive'] = $this->m_data->getData('Todo_id, Todo_nametask, Todo_tanggalduedate, Todo_deskripsi, Todo_statustask', array('Todo_statustask' => 'archive'));
		$this->m_data->updateProgress(); 
		$data['content'] = 'template/content';
		$this->load->view('v_todo', $data);
	}

	public function add_task(){
		$data['content'] = 'v_add_task';
		$this->load->view('v_todo', $data);
	}

	public function edit_task($id){
		$data['task'] = $this->m_data->getData('Todo_id, Todo_nametask, Todo_tanggalduedate, Todo_deskripsi, Todo_statustask', NULL, array('Todo_id' => $id));
		$data['content'] = 'v_edit_task';
		$this->load->view('v_todo', $data);
	}

	public function detail_task($id){
		$data['task'] = $this->m_data->getData('Todo_id, Todo_nametask, Todo_tanggalduedate, Todo_deskripsi, Todo_statustask', NULL, array('Todo_id' => $id));
		$data['content'] = 'v_detail_task';
		$this->load->view('v_todo', $data);
	}

	public function update_status_task($kode)
	{
		$value = $this->m_data->getData('Todo_statustask', NULL, array('Todo_id' => $kode));
		if ($value->Todo_statustask == "new") {
			$this->m_data->updateData(array('Todo_statustask' => 'progress'), 'Todo_id', $kode);
		} else if ($value->Todo_statustask == "progress") {
			$this->m_data->updateData(array('Todo_statustask' => 'done'), 'Todo_id', $kode);
		}
		$this->session->set_flashdata('messages_status', 'Update status sukses.');
		return $this->index();
	}

	public function archive_task($kode)
	{
		if ($kode == TRUE) {
			$this->m_data->updateData(array('Todo_statustask' => 'archive'), 'Todo_id', $kode);
			$this->session->set_flashdata('messages_status', 'Status task archive.');
		} else {
			$this->session->set_flashdata('messages_status', 'Update status gagal.');
		}
		return $this->index();
	}

	public function simpan_task()
	{
		$this->form_validation->set_rules('namatask', 'nama task', 'required');
		$this->form_validation->set_rules('tanggal', 'tanggal', 'required');
		$this->form_validation->set_rules('deskripsi', 'deskripsi', 'required');
		$this->form_validation->set_rules('statustask', 'status', 'required');
		$namatask = $this->input->post('namatask');
		$tanggal = $this->input->post('tanggal');
		$deskripsi = $this->input->post('deskripsi');
		$status = $this->input->post('statustask');

		if ($this->form_validation->run() == FALSE) {
			$this->add_task();
		} else {
			$data = array(
				'Todo_nametask' => $namatask, 
				'Todo_tanggalduedate' => $tanggal, 
				'Todo_deskripsi' => $deskripsi, 
				'Todo_statustask' => $status 
			);
			$this->db->insert('t_todo', $data);
			$this->session->set_flashdata('success', 'Task berhasil di tambah.');
			return $this->index();
		}
	}

	public function update_task()
	{
		$this->form_validation->set_rules('namatask', 'nama task', 'required');
		$this->form_validation->set_rules('tanggal', 'tanggal', 'required');
		$this->form_validation->set_rules('deskripsi', 'deskripsi', 'required');
		$this->form_validation->set_rules('statustask', 'status', 'required');
		$id = $this->input->post('id');
		$namatask = $this->input->post('namatask');
		$tanggal = $this->input->post('tanggal');
		$deskripsi = $this->input->post('deskripsi');
		$status = $this->input->post('statustask');

		if ($this->form_validation->run() == FALSE) {
			$this->edit_task($id);
		} else {
			$data = array(
				'Todo_nametask' => $namatask, 
				'Todo_tanggalduedate' => $tanggal, 
				'Todo_deskripsi' => $deskripsi, 
				'Todo_statustask' => $status 
			);
			$this->db->update('t_todo', $data, array('Todo_id' => $id));
			$this->session->set_flashdata('success', 'Task berhasil di update.');
			return $this->index();
		}
	}
}