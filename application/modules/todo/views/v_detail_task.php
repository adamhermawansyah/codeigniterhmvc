<section id="main-content">
  <section class="wrapper">
   <h3> To-Do Lists</h3>


   <!-- COMPLEX TO DO LIST -->     
   <div class="row mt">
    <div class="col-md-12">
    <section class="task-panel tasks-widget">
      <div class="panel-heading">
       <div class="pull-left"><h5><i class="fa fa-tasks"></i> Task Detail </h5></div>
       <br>
     </div>
     <div class="panel-body">
      <div class="task-content">

        <ul class="task-list">
            <li>
              <div class="task-title">
                <span class="task-title-sp">Nama Task : </span>
                <span class="task-title-sp"><?= $task->Todo_nametask ?></span>
              </div>
            </li>   
            <li>
              <div class="task-title">
                <span class="task-title-sp">Tanggal Task : </span>
                <span class="task-title-sp"><?= $task->Todo_tanggalduedate ?></span>
              </div>
            </li>   
            <li>
              <div class="task-title">
                <span class="task-title-sp">Deskripsi Task : </span>
                <span class="task-title-sp"><?= $task->Todo_deskripsi ?></span>
              </div>
            </li>   
            <li>
              <div class="task-title">
                <span class="task-title-sp">Status Task : </span>
                <span class="task-title-sp"><?= $task->Todo_statustask ?></span>
              </div>
            </li>                            
        </ul>
      </div>

      <div class=" add-task-row">
        <a class="btn btn-default btn-sm pull-left" href="<?= site_url('todo/todoapp') ?>">Back</a>
      </div>
    </div>
  </section>
</div><!-- /col-md-12-->
</div><!-- /row -->


</section>
</section><!-- /MAIN CONTENT 