<section id="main-content">
  <section class="wrapper">
   <h3> To-Do Lists</h3>

   
   <!-- COMPLEX TO DO LIST -->			
   <div class="row mt">
    <div class="col-md-7">
      <section class="task-panel tasks-widget">
        <div class="panel-heading">
         <div class="pull-left"><h5><i class="fa fa-tasks"></i> Add Task </h5></div>
         <br>
       </div>
       <div class="panel-body">
        <div class="task-content">
          <form class="form" action="<?= site_url('todo/todoapp/simpan_task') ?>" method="post">
            <div class="form-group">
              <label for="namatask" class="text-info">Task Name</label><br>
              <input type="text" name="namatask" id="namatask" class="form-control" placeholder="nama task">
              <a style="color: red;"><?= form_error('namatask') ?></a>
            </div>
            <div class="form-group">
              <label for="tanggal" class="text-info">Task Tanggal</label><br>
              <input type="date" name="tanggal" id="tanggal" class="form-control">
              <a style="color: red;"><?= form_error('tanggal') ?></a>
            </div>
            <div class="form-group">
              <label for="deskripsi" class="text-info">Task Deskripsi</label><br>
             <textarea name="deskripsi" id="deskripsi" class="form-control" rows="5" placeholder="deskripsi task"></textarea>
              <a style="color: red;"><?= form_error('deskripsi') ?></a>
            </div>
            <div class="form-group">
              <label for="statustask" class="text-info">Task Status</label><br>
              <select name="statustask" id="statustask" class="form-control" style="width: 30%;">
                <option value="">Pilih</option>
                <option value="new">New</option>
                <option value="progress">Progress</option>
                <option value="done">Done</option>
              </select>
              <a style="color: red;"><?= form_error('statustask') ?></a>
            </div>
            <div class="form-group">
              <!-- <label for="remember-me" class="text-info"><span>Remember me</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label><br> -->
              <input type="submit" name="submit" class="btn btn-info btn-md" value="Submit">
            </div>
          </form>
        </div>

        <div class="add-task-row">
          <a class="btn btn-default btn-sm pull-left" href="<?= site_url('todo/todoapp') ?>">Back</a>
        </div>
      </div>
    </section>
  </div><!-- /col-md-12-->
</div><!-- /row -->

</section><! --/wrapper -->
</section><!-- /MAIN CONTENT 