<section id="main-content">
  <section class="wrapper">
   <h3> To-Do Lists</h3>


   <!-- COMPLEX TO DO LIST -->     
   <div class="row mt">
    <div class="col-md-12">
     <?php if ($this->session->flashdata('success')) { ?>
      <div class="alert alert-success"><?= $this->session->flashdata('success') ?></div>
    <?php } else if ($this->session->flashdata('messages_status')) {?>
      <div class="alert alert-success"><?= $this->session->flashdata('messages_status') ?></div>
    <?php } ?>
    <section class="task-panel tasks-widget">
      <div class="panel-heading">
       <div class="pull-left"><h5><i class="fa fa-tasks"></i> Task List </h5></div>
       <br>
     </div>
     <div class="panel-body">
      <div class="task-content">

        <ul class="task-list">
          <?php foreach ($task_list as $value) { ?>
            <li>
              <div class="task-checkbox">
                <input type="checkbox" class="list-child" value=""  />
              </div>
              <div class="task-title">
                <span class="task-title-sp"><?= $value->Todo_nametask ?></span>
                <span class="badge bg-theme"><?= $value->Todo_tanggalduedate ?></span>
                <span class="badge bg-theme"><?= $value->Todo_statustask ?></span>
                <div class="pull-right hidden-phone">
                  <a href="<?= site_url('todo/todoapp/detail_task/'. $value->Todo_id) ?>" class="btn btn-success btn-xs"><i class=" fa fa-check"></i></a>
                  <a onclick="return confirm('Anda yakin ingin update data?');" href="<?= site_url('todo/todoapp/edit_task/'. $value->Todo_id) ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                  <a onclick="return confirm('Anda yakin ingin update status task?');" href="<?= site_url('todo/todoapp/update_status_task/'. $value->Todo_id) ?>" class="btn btn-default btn-xs"><i class="fa fa-pencil"></i></a>
                  <a onclick="return confirm('Anda yakin ingin hapus?');" href="<?= site_url('todo/todoapp/archive_task/'. $value->Todo_id) ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                </div>
              </div>
            </li>  
          <?php } ?>                            
        </ul>
      </div>

      <div class=" add-task-row">
        <a class="btn btn-success btn-sm pull-left" href="<?= site_url('todo/todoapp/add_task') ?>">Add New Tasks</a>
        <!-- <a class="btn btn-default btn-sm pull-right" href="todo_list.html#">See All Tasks</a> -->
      </div>
    </div>
  </section>
</div><!-- /col-md-12-->
</div><!-- /row -->


<!-- COMPLEX TO DO LIST -->			
<div class="row mt">
  <div class="col-md-12">
    <section class="task-panel tasks-widget">
      <div class="panel-heading">
       <div class="pull-left"><h5><i class="fa fa-tasks"></i> Task Done </h5></div>
       <br>
     </div>
     <div class="panel-body">
      <div class="task-content">

        <ul class="task-list">
          <?php foreach ($task_done as $value) { ?>
            <li>
              <div class="task-checkbox">
                <input type="checkbox" class="list-child" value=""  />
              </div>
              <div class="task-title">
                <span class="task-title-sp"><?= $value->Todo_nametask ?></span>
                <span class="badge bg-theme"><?= $value->Todo_tanggalduedate ?></span>
                <span class="badge bg-success"><?= $value->Todo_statustask ?></span>
                <div class="pull-right hidden-phone">
                  <a href="<?= site_url('todo/todoapp/detail_task/'. $value->Todo_id) ?>" class="btn btn-success btn-xs"><i class=" fa fa-check"></i></a>
                  <a onclick="return confirm('Anda yakin ingin update data?');" href="<?= site_url('todo/todoapp/edit_task/'. $value->Todo_id) ?>" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                  <a onclick="return confirm('Anda yakin ingin hapus?');" href="<?= site_url('todo/todoapp/archive_task/'. $value->Todo_id) ?>" class="btn btn-danger btn-xs"><i class="fa fa-trash-o "></i></a>
                </div>
              </div>
            </li>  
          <?php } ?>                            
        </ul>
      </div>
    </div>
  </section>
</div><!-- /col-md-12-->
</div><!-- /row -->


<!-- SORTABLE TO DO LIST -->

<div class="row mt mb">
  <div class="col-md-12">
    <section class="task-panel tasks-widget">
      <div class="panel-heading">
       <div class="pull-left"><h5><i class="fa fa-tasks"></i> Task Archive </h5></div>
       <br>
     </div>
     <div class="panel-body">
      <div class="task-content">
        <ul id="sortable" class="task-list">
          <?php foreach ($task_archive as $value) { ?>
            <li class="list-warning">
              <i class=" fa fa-ellipsis-v"></i>
              <div class="task-checkbox">
                <input type="checkbox" class="list-child" value=""  />
              </div>
              <div class="task-title">
                <span class="task-title-sp"><?= $value->Todo_nametask ?></span>
                <span class="badge bg-theme"><?= $value->Todo_tanggalduedate ?></span>
                <span class="badge bg-important"><?= $value->Todo_statustask ?></span>
                <!-- <div class="pull-right hidden-phone">
                  <button class="btn btn-success btn-xs fa fa-check"></button>
                  <button class="btn btn-primary btn-xs fa fa-pencil"></button>
                  <button class="btn btn-danger btn-xs fa fa-trash-o"></button>
                </div> -->
              </div>
            </li>
          <?php } ?>      
        </ul>
      </div>
    <!-- <div class=" add-task-row">
      <a class="btn btn-success btn-sm pull-left" href="todo_list.html#">Add New Tasks</a>
      <a class="btn btn-default btn-sm pull-right" href="todo_list.html#">See All Tasks</a>
    </div> -->
  </div>
</section>
</div><!--/col-md-12 -->
</div><!-- /row -->

</section><! --/wrapper -->
</section><!-- /MAIN CONTENT 